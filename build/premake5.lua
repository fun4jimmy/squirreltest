------------------------------------------------------------------------------------------------------------------------
-- squirreltest library
------------------------------------------------------------------------------------------------------------------------

------------------------------------------------------------------------------------------------------------------------
-- include required scripts
------------------------------------------------------------------------------------------------------------------------
dofile "embed.lua"

------------------------------------------------------------------------------------------------------------------------
-- option: --to=<path>
------------------------------------------------------------------------------------------------------------------------
newoption {
  trigger     = "to",
  description = "Specify the output path for generated projects.",
  value       = "path",
}

------------------------------------------------------------------------------------------------------------------------
-- option: --squirrel=<path>
------------------------------------------------------------------------------------------------------------------------
newoption {
  trigger     = "squirrel",
  description = "Specify the location of squirrel.",
  value       = "path",
}

------------------------------------------------------------------------------------------------------------------------
-- option: --gtest=<path>
------------------------------------------------------------------------------------------------------------------------
newoption {
  trigger = "gtest",
  description = "Specify the location of googletest.",
  value   = "path",
}

------------------------------------------------------------------------------------------------------------------------
-- option: --outdir=<path> specifies the output path for embed
------------------------------------------------------------------------------------------------------------------------
newoption {
  trigger = "outdir",
  description = "Specify the output directory for the embed action.",
  value   = "path",
}

------------------------------------------------------------------------------------------------------------------------
-- action: embed
------------------------------------------------------------------------------------------------------------------------
newaction {
  trigger     = "embed",
  description = "Embed a test script as a C header",
  execute = function()
    do_embed_action(_ARGV)
  end
}

if _ACTION == "embed" then
  return
end

------------------------------------------------------------------------------------------------------------------------
-- action: release
------------------------------------------------------------------------------------------------------------------------
newaction {
   trigger     = "release",
   description = "Build a release package for the project",
   execute = function ()
      local cwd = os.getcwd()
      local tmp = path.getname(os.tmpname())

      os.mkdir(tmp)
      os.chdir(tmp)
      os.execute(string.format("svn export %s sqtest", path.getrelative(os.getcwd(), project_directory)))

      os.execute(string.format("%s --file=sqtest/build/premake4.lua vs2008", premake4, path.join(export, "build/premake4.lua")))
      os.execute(string.format("%s --file=sqtest/build/premake4.lua gmake", premake4, path.join(export, "build/premake4.lua")))
      os.execute("tar -czf ../sqtest.tar.gz sqtest")
      os.chdir(cwd)
      os.rmdir(tmp)
   end
}

------------------------------------------------------------------------------------------------------------------------
-- platforms
------------------------------------------------------------------------------------------------------------------------
Win32_x86_32 = "Win32-x86_32"
Win32_x86_64 = "Win32-x86_64"
MacOSX_Universal = "MacOSX_Universal"

------------------------------------------------------------------------------------------------------------------------
-- configurations
------------------------------------------------------------------------------------------------------------------------
configs = {
  "debug",
  "debug-unicode",
  "release",
  "release-unicode",
}

------------------------------------------------------------------------------------------------------------------------
-- actions
------------------------------------------------------------------------------------------------------------------------
actions = { }

actions.vs2012 = {
  platforms = { Win32_x86_32, Win32_x86_64, },
  configurations = configs,
}
actions.vs2013 = {
  platforms = { Win32_x86_32, Win32_x86_64, },
  configurations = configs,
}
  
if os.get() == "windows" then
  actions.gmake = {
    platforms = { Win32_x86_32, Win32_x86_64, },
    configurations = configs,
  }
elseif os.get() == "macosx" then
  actions.gmake = {
    platforms = { MacOSX_Universal, },
    configurations = configs,
  }
end

------------------------------------------------------------------------------------------------------------------------
-- output directories
------------------------------------------------------------------------------------------------------------------------
local project_directory = _OPTIONS["to"] or _ACTION
local intermediate_directory = path.join("..", "obj", _ACTION, "%{prj.name}", "%{cfg.platform}", "%{cfg.buildcfg}")
local library_directory = path.join("..", "lib", _ACTION, "%{cfg.platform}", "%{cfg.buildcfg}")
local binary_directory = path.join("..", "bin", _ACTION, "%{cfg.platform}", "%{cfg.buildcfg}")

------------------------------------------------------------------------------------------------------------------------
-- squirreltest solution
------------------------------------------------------------------------------------------------------------------------
solution "squirreltest"
  location( project_directory )
  platforms( actions[_ACTION].platforms )
  configurations( actions[_ACTION].configurations )

  -- setup the object files directory
  --
  objdir( intermediate_directory )

  -- set some common configuration properties
  --
  configuration{ }
    warnings "extra"
    flags {
      "NoEditAndContinue",
      "FatalWarnings",
    }

  -- visual studio 2012 beta needs this define to fix a bug with tuple
  --
  if _ACTION == "vs2012" then
    defines{ "_VARIADIC_MAX=10", }
  end

  configuration "x64"
    defines{ "_SQ64", }

  configuration{ Win32_x86_32, }
    architecture "x32"

  configuration{ Win32_x86_64, }
    architecture "x64"

  configuration{ MacOSX_Universal, }
    architecture "universal"
    toolset "clang"

  configuration{ "debug or debug-unicode", }
    optimize "Off"
    defines {
      "DEBUG",
      "_DEBUG",
    }

    flags {
      "Symbols",
    }

  configuration{ "release or release-unicode", }
    optimize "Full"

  configuration{ "debug-unicode or release-unicode", }
    flags {
      "Unicode",
    }

  if _OPTIONS["gtest"] then
    local gtest_location = _OPTIONS["gtest"]

    -- googletest library
    --
    project "gtest"
      location( project_directory )
      kind "StaticLib"
      uuid( os.uuid("gtest") )
      language "C++"
      files {
        _SCRIPT,
        path.join(gtest_location, "include", "**.h"),
        path.join(gtest_location, "src", "gtest-all.cc"),
      }
      vpaths {
        ["build"] = { "*.lua", },
        ["*"] = { path.join(gtest_location, "**"), },
      }
      includedirs {
        path.join(gtest_location, "include"),
        path.join(gtest_location),
      }
      targetdir( library_directory )
      warnings "Off"
  end

  if _OPTIONS["squirrel"] then
    local squirrel_location = _OPTIONS["squirrel"]

    -- squirrel library
    --
    project "squirrel"
      location( project_directory )
      uuid( os.uuid("squirrel") )
      kind "StaticLib"
      language "C++"
      files {
        _SCRIPT,
        path.join(squirrel_location, "include", "squirrel.h"),
        path.join(squirrel_location, "squirrel", "**.cpp"),
        path.join(squirrel_location, "squirrel", "**.h"),
      }
      vpaths {
        ["build"] = { "*.lua", },
        ["*"] = { path.join(squirrel_location, "**"), },
      }
      includedirs {
        path.join(squirrel_location, "include"),
      }
      targetdir( library_directory )
      warnings "Off"

    -- sqstdlib library
    --
    project "sqstdlib"
      location( project_directory )
      uuid( os.uuid("sqstdlib") )
      kind "StaticLib"
      language "C++"
      files {
        _SCRIPT,
        path.join(squirrel_location, "include", "sqstd*.h"),
        path.join(squirrel_location, "sqstdlib", "**.cpp"),
        path.join(squirrel_location, "sqstdlib", "**.h"),
      }
      vpaths {
        ["build"] = { "*.lua", },
        ["*"] = { path.join(squirrel_location, "**"), },
      }
      includedirs {
        path.join(squirrel_location, "include"),
      }
      targetdir( library_directory )
      warnings "Off"
  end

  -- squirreltest library
  --
  project "squirreltest"
    location( project_directory )
    uuid( os.uuid("squirreltest") )
    kind "StaticLib"
    language "C++"
    files {
      _SCRIPT,
      path.join("..", "include", "sqtest", "*.h"),
      path.join("..", "src", "*.cpp"),
    }
    vpaths {
      ["build"] = { "*.lua", },
      ["*"] = { "../**" },
    }
    includedirs {
      path.join("..", "include"),
    }
    if _OPTIONS["squirrel"] then
      includedirs {
        path.join(_OPTIONS["squirrel"], "include"),
      }
    end
    if _OPTIONS["gtest"] then
      includedirs {
        path.join(_OPTIONS["gtest"], "include"),
      }
    end
    targetdir( library_directory )

  -- unit test
  --
  project "squirreltest-test"
    location( project_directory )
    uuid( os.uuid("squirreltest-test") )
    kind "ConsoleApp"
    language "C++"
    files {
      _SCRIPT,
      path.join("..", "test", "**.h"),
      path.join("..", "test", "**.inl"),
      path.join("..", "test", "**.cpp"),
      path.join("..", "test", "scripts", "*.nut"),
    }
    vpaths {
      ["build"] = { "*.lua", },
      ["*"] = { "../**" },
    }
    includedirs {
      path.join("..", "include"),
      path.join("..", "test"),
    }
    if _OPTIONS["squirrel"] then
      includedirs {
        path.join(_OPTIONS["squirrel"], "include"),
      }
    end
    if _OPTIONS["gtest"] then
      includedirs {
        path.join(_OPTIONS["gtest"], "include"),
      }
    end
    libdirs {
      library_directory,
    }
    targetdir( binary_directory )

    links {
      "gtest",
      "squirrel",
      "sqstdlib",
      "squirreltest",
    }

    local embed_pattern = path.join("..", "test", "scripts", "*.nut")
    local embed_directory = path.join("..", "..", "test", "embedded")
    embed(embed_pattern, embed_directory)

  -- testapp executable
  --
  project "squirreltest-runner"
    location( project_directory )
    uuid( os.uuid("squirreltest-runner") )
    kind "ConsoleApp"
    language "C++"
    targetname "sqtest"
    files {
      _SCRIPT,
      path.join("..", "runner", "*.cpp"),
      path.join("..", "scripts", "*.nut"),
    }
    vpaths {
      ["build"] = { "*.lua", },
      ["*"] = { path.join("..", "**") },
    }
    includedirs {
      path.join("..", "include"),
    }
    if _OPTIONS["squirrel"] then
      includedirs {
        path.join(_OPTIONS["squirrel"], "include"),
      }
    end
    if _OPTIONS["gtest"] then
      includedirs {
        path.join(_OPTIONS["gtest"], "include"),
      }
    end
    libdirs {
      library_directory,
    }
    targetdir( binary_directory )

    links {
      "gtest",
      "squirrel",
      "sqstdlib",
      "squirreltest",
    }