# A Google Test binding for Squirrel #

Exposes some of the [**Google Test**](https://github.com/google/googletest) framework to the scripting language [**Squirrel**](http://squirrel-lang.org/).

## Getting Started ##

After downloading **Squirrel Test**, unpack it, and read through the code in **testapp/main.cpp** and the example scripts in the scripts directory.

## Requirements ##

**Squirrel Test** is dependent on **Google Test** (version 1.5 or later) and **Squirrel** (version 3.0 or later) so you will need to get both those libraries as well. The assumed directory layout is:

* **$(RootDir)/squirrel**
* **$(RootDir)/googletest**
* **$(RootDir)/sqtest**

If your layout differs from this you will have to edit the project options. If you have [**premake**](https://premake.github.io/) you can open up **build/premake4.lua** and edit it to point at the correct locations. You can also generate other project file types (ie xcode) using this configuration file.

It may work with **Google Test** versions earlier than 1.5 but it has not been tested.

It should work with 2.x versions of **Squirrel** as it does not rely on of the new features of **Squirrel 3.0** but again it has not been tested.

## Usage ##

The most basic code to test a script file named **fibonacci.nut** is:

### main.cpp: ###
```
#!c++

#include <gtest/gtest.hpp>

#include <sqtest/sqtest.h>

int main(int argc, char **argv)
{
  sqtest_addfile("fibonacci.nut");
  ::testing::!InitGoogleTest(&argc, argv);
  return RUN_ALL_TESTS();
}
```

### fibonacci.nut: ###
```
#!squirrel

function fibonacci(n)
{
  if (n < 2)
  {
     return 1;
  }
  else
  {
    return fibonacci(n - 2) + fibonacci(n - 1);
  }
}

test.zeroReturnsOne <- function()
{
  expect_eq(1, fibonacci(0));
}

test.oneReturnsOne <- function()
{
  expect_eq(1, fibonacci(1));
}

test.fiveReturnsEight <- function()
{
  expect_eq(8, fibonacci(5));
}
```

## Supported Assertions ##

| Google Test C++ Macro | Squirrel Test Script Function | Description |
|:--------------------------|:----------------------------------|:----------------|
| `EXPECT_TRUE(condition)` | `expect_true(condition)` | Test if condition is true, generates nonfatal failure |
| `ASSERT_TRUE(condition)` | `assert_true(condition)` | Test if condition is true, generates fatal failure |
| `EXPECT_FALSE(condition)` | `expect_false(condition)` | Test if condition is false, generates nonfatal failure |
| `ASSERT_FALSE(condition)` | `assert_false(condition)` | Test if condition is false, generates fatal failure |
| `EXPECT_EQ(expected, actual)` | `expect_eq(expected, actual)` | Test if expected and actual are equal, generates nonfatal failure |
| `ASSERT_EQ(expected, actual)` | `assert_eq(expected, actual)` | Test if expected and actual are equal, generates fatal failure |
| `EXPECT_NE(expected, actual)` | `expect_ne(expected, actual)` | Test if expected and actual are not equal, generates nonfatal failure |
| `ASSERT_NE(expected, actual)` | `assert_ne(expected, actual)` | Test if expected and actual are not equal, generates fatal failure |
| `EXPECT_LT(expected, actual)` | `expect_lt(expected, actual)` | Test if expected is less than actual, generates nonfatal failure |
| `ASSERT_LT(expected, actual)` | `assert_lt(expected, actual)` | Test if expected is less than actual, generates fatal failure |
| `EXPECT_LE(expected, actual)` | `expect_le(expected, actual)` | Test if expected is less than or equal to actual, generates nonfatal failure |
| `ASSERT_LE(expected, actual)` | `assert_le(expected, actual)` | Test if expected is less than or equal to actual, generates fatal failure |
| `EXPECT_GT(expected, actual)` | `expect_gt(expected, actual)` | Test if expected is greater than actual, generates nonfatal failure |
| `ASSERT_GT(expected, actual)` | `assert_gt(expected, actual)` | Test if expected is greater than actual, generates fatal failure |
| `EXPECT_GE(expected, actual)` | `expect_ge(expected, actual)` | Test if expected is greater than or equal to actual, generates nonfatal failure |
| `ASSERT_GE(expected, actual)` | `assert_ge(expected, actual)` | Test if expected is greater than or equal to actual, generates fatal failure |
| `EXPECT_STREQ(expected, actual)` | `expect_streq(expected, actual)` | Test if two strings are equal, generates nonfatal failure |
| `ASSERT_STREQ(expected, actual)` | `assert_streq(expected, actual)` | Test if two strings are equal, generates fatal failure |
| `EXPECT_STRNE(expected, actual)` | `expect_strne(expected, actual)` | Test if two strings are not equal, generates nonfatal failure |
| `ASSERT_STRNE(expected, actual)` | `assert_strne(expected, actual)` | Test if two strings are not equal, generates fatal failure |
| `EXPECT_STRCASEEQ(expected, actual)` | `expect_strcaseeq(expected, actual)` | Test if two strings are equal ignoring case, generates nonfatal failure |
| `ASSERT_STRCASEEQ(expected, actual)` | `assert_strcaseeq(expected, actual)` | Test if two strings are equal ignoring case, generates fatal failure |
| `EXPECT_STRCASENE(expected, actual)` | `expect_strcasene(expected, actual)` | Test if two strings are not equal ignoring case, generates nonfatal failure |
| `ASSERT_STRCASENE(expected, actual)` | `assert_strcasene(expected, actual)` | Test if two strings are not equal ignoring case, generates fatal failure |
| `EXPECT_FLOAT_EQ(expected, actual)` | `expect_float_eq(expected, actual)` | Test if two floating point values are equal, generates nonfatal failure |
| `ASSERT_FLOAT_EQ(expected, actual)` | `assert_float_eq(expected, actual)` | Test if two floating point values are equal, generates fatal failure |