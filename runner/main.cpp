/*
Copyright (c) 2011 James Whitworth

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.

*/
#include <stdio.h>
#include <sys/stat.h>

#include <gtest/gtest.h>

#include <sqtest/sqtest.h>

//----------------------------------------------------------------------------------------------------------------------
SQRESULT file_exists(const char* filename)
{
  struct stat buffer;

  int result = stat(filename, &buffer);
  return (result == 0) ? SQTrue : SQFalse;
}

//----------------------------------------------------------------------------------------------------------------------
int main(int argc, char** argv)
{
  const char* filename = NULL;

  if (argc != 2)
  {
    fprintf(stderr, "usage: test \"scripts/test.nut\".\n");
    return 1;
  }

  filename = argv[1];

  if (SQ_FAILED(file_exists(filename)))
  {
    fprintf(stderr, "script file \"%s\" does not exist.\n", filename);
    return 1;
  }

  if (SQ_FAILED(sqtest_addfile(filename)))
  {
    return 1;
  }

  testing::InitGoogleTest(&argc, argv);
  int retcode = RUN_ALL_TESTS();
  return retcode;
}
