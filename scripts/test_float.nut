local number = 1.5;

expect_float_eq(1.5, number);
expect_float_eq(1.6, number);

assert_float_eq(1.5, number);
assert_float_eq(1.6, number);