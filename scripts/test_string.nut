local str = "this is a string";

expect_streq("this is a string", str);
expect_streq("this will fail", str);

expect_strneq("this will fail", str);
expect_strneq("this is a string", str);