local value = true

// this will pass
expect_true(value);

// this will fail
expect_false(value);

// this will pass
assert_true(value);

// this will fail and also end the test
assert_false(value);

// any code here will not execute