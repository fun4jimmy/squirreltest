
local value = 5;

// this will pass
expect_eq(5, value);
// this will fail
expect_eq(6, value);

// this will pass
expect_gt(value, 1);
// this will fail
expect_gt(value, 6);

// this will pass
expect_lt(value, 10);
// this will fail
expect_lt(value, 4);