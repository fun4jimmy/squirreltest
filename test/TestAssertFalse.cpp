//----------------------------------------------------------------------------------------------------------------------
// Copyright (c) 2013 James Whitworth
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//----------------------------------------------------------------------------------------------------------------------
#include "fixtures/SquirrelTestFixture.h"
//----------------------------------------------------------------------------------------------------------------------
#include <gtest/gtest-spi.h>
//----------------------------------------------------------------------------------------------------------------------
#include <sqtest/sqtest.h>
//----------------------------------------------------------------------------------------------------------------------

typedef SquirrelTestFixture TestAssertFalse;

//----------------------------------------------------------------------------------------------------------------------
// TestAssertFalse
//----------------------------------------------------------------------------------------------------------------------
TEST_F(TestAssertFalse, PassTest)
{
  // arrange
  //
  sq_pushroottable(m_vm);
  sq_pushbool(m_vm, SQFalse);

  // act
  //
  auto result = sqtest_assert_false(m_vm);

  // assert
  //
  EXPECT_EQ(0, result);
}

//----------------------------------------------------------------------------------------------------------------------
void CallAssertFalseWithSQTrue()
{
  // arrange
  //
  auto vm = sq_open(1024);

  sq_pushroottable(vm);
  sq_pushbool(vm, SQTrue);

  // act and assert
  //
  sqtest_assert_false(vm);

  // cleanup
  //
  sq_close(vm);
}

//----------------------------------------------------------------------------------------------------------------------
TEST_F(TestAssertFalse, FailTest)
{
  // have to do this with a static function for EXPECT_FATAL_FAILURE to work
  //
  EXPECT_FATAL_FAILURE(CallAssertFalseWithSQTrue(), "");
}

//----------------------------------------------------------------------------------------------------------------------
TEST_F(TestAssertFalse, PassScriptTest)
{
  // arrange
  //
  static auto buffer = _SC("assert_false(false)");

  // act and assert
  //
  EXPECT_NO_FATAL_FAILURE(CompileAndExecuteBuffer(buffer));
}

//----------------------------------------------------------------------------------------------------------------------
TEST_F(TestAssertFalse, FailScriptTest)
{
  // arrange
  //
  static auto buffer = _SC("assert_false(true)");

  // act and assert
  //
  EXPECT_FATAL_FAILURE(CompileAndExecuteBuffer(buffer), "");
}
