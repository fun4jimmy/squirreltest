//----------------------------------------------------------------------------------------------------------------------
// Copyright (c) 2013 James Whitworth
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//----------------------------------------------------------------------------------------------------------------------
#include "fixtures/SquirrelFixture.h"
//----------------------------------------------------------------------------------------------------------------------
#include <sqtest/sqtest.h>
//----------------------------------------------------------------------------------------------------------------------

class TestRegister : public SquirrelFixture, public testing::WithParamInterface<const SQChar *> { };

//----------------------------------------------------------------------------------------------------------------------
// TestRegister
//----------------------------------------------------------------------------------------------------------------------
TEST_F(TestRegister, ReturnSuccessTest)
{
  // arrange and act
  //
  sq_pushroottable(m_vm);
  auto result = sqtest_register_lib(m_vm);
  sq_poptop(m_vm);

  // assert
  //
  EXPECT_TRUE(SQ_SUCCEEDED(result));
}

#if GTEST_HAS_PARAM_TEST

//----------------------------------------------------------------------------------------------------------------------
TEST_P(TestRegister, FunctionRegisteredTest)
{
  // arrange and act
  //
  sq_pushroottable(m_vm);
  sqtest_register_lib(m_vm);
  sq_poptop(m_vm);

  // assert
  //
  auto function = GetParam();
  EXPECT_PRED2(CheckFunctionExists, m_vm, function);
}

//----------------------------------------------------------------------------------------------------------------------
const SQChar *kExpectFunctionNames[] = {
  _SC("expect_true"),
  _SC("expect_false"),
  _SC("expect_eq"),
  _SC("expect_ne"),
  _SC("expect_lt"),
  _SC("expect_le"),
  _SC("expect_gt"),
  _SC("expect_ge"),
  _SC("expect_streq"),
  _SC("expect_strne"),
  _SC("expect_strcaseeq"),
  _SC("expect_strcasene"),
  _SC("expect_float_eq"),
};
INSTANTIATE_TEST_CASE_P(ExpectFunctions, TestRegister, testing::ValuesIn(kExpectFunctionNames));

//----------------------------------------------------------------------------------------------------------------------
const SQChar *kAssertFunctionNames[] = {
  _SC("assert_true"),
  _SC("assert_false"),
  _SC("assert_eq"),
  _SC("assert_ne"),
  _SC("assert_lt"),
  _SC("assert_le"),
  _SC("assert_gt"),
  _SC("assert_ge"),
  _SC("assert_streq"),
  _SC("assert_strne"),
  _SC("expect_strcaseeq"),
  _SC("expect_strcasene"),
  _SC("expect_float_eq"),
};
INSTANTIATE_TEST_CASE_P(AssertFunctions, TestRegister, testing::ValuesIn(kAssertFunctionNames));

#else

TEST(DummyTest, ValueParameterizedTestsAreNotSupportedOnThisPlatform) {}

#endif
