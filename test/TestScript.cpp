//----------------------------------------------------------------------------------------------------------------------
// Copyright (c) 2013 James Whitworth
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//----------------------------------------------------------------------------------------------------------------------
#include "fixtures/SquirrelFixture.h"
//----------------------------------------------------------------------------------------------------------------------
#include <sqtest/sqtScript.h>
//----------------------------------------------------------------------------------------------------------------------

typedef SquirrelFixture TestScript;

//----------------------------------------------------------------------------------------------------------------------
// TestScript
//----------------------------------------------------------------------------------------------------------------------
TEST_F(TestScript, EmptyScriptTest)
{
  // arrange and act
  //
  sqt::Script script;

  // assert
  //
  EXPECT_EQ(nullptr, script.GetContents());
  size_t length;
  EXPECT_TRUE(script.GetContents(&length) == nullptr);
  EXPECT_EQ(0u, length);
  EXPECT_EQ(0u, script.GetLineCount());
  EXPECT_EQ(sqt::Script::kParseEmptyContents, script.ParseContents());
  EXPECT_EQ(0u, script.GetTestCount());
}

//----------------------------------------------------------------------------------------------------------------------
TEST_F(TestScript, ParseContentsSuccessTest)
{
  // arrange
  //
  auto kScriptContents =
    _SC("test.one <- function() { }")
    _SC("test.two <- function() { }");
  sqt::Script script;
  script.SetContents(kScriptContents);

  // act
  //
  auto result = script.ParseContents();

  // assert
  //
  EXPECT_EQ(sqt::Script::kParseSuccess, result);
  EXPECT_EQ(2u, script.GetTestCount());
}

//----------------------------------------------------------------------------------------------------------------------
TEST_F(TestScript, ParseContentsEmptyTest)
{
  // arrange
  //
  sqt::Script script;

  // act
  //
  auto result = script.ParseContents();

  // assert
  //
  EXPECT_EQ(sqt::Script::kParseEmptyContents, result);
  EXPECT_EQ(0u, script.GetTestCount());
}

//----------------------------------------------------------------------------------------------------------------------
TEST_F(TestScript, ParseContentsCompileErrorTest)
{
  // arrange
  //
  auto kScriptContents = _SC("{");
  sqt::Script script;
  script.SetContents(kScriptContents);

  // act
  //
  auto result = script.ParseContents();

  // assert
  //
  EXPECT_EQ(sqt::Script::kParseCompilationError, result);
  EXPECT_EQ(0u, script.GetTestCount());
}

//----------------------------------------------------------------------------------------------------------------------
TEST_F(TestScript, ParseContentsExecutionErrorTest)
{
  // arrange
  //
  auto kScriptContents = _SC("callMissingFunction()");
  sqt::Script script;
  script.SetContents(kScriptContents);

  // act
  //
  auto result = script.ParseContents();

  // assert
  //
  EXPECT_EQ(sqt::Script::kParseExecutionError, result);
  EXPECT_EQ(0u, script.GetTestCount());
}
