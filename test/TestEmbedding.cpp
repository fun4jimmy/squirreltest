//----------------------------------------------------------------------------------------------------------------------
// Copyright (c) 2013 James Whitworth
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//----------------------------------------------------------------------------------------------------------------------
#include <sqtest/sqtEmbeddedTestCase.h>
//----------------------------------------------------------------------------------------------------------------------
#include "fixtures/EmbeddingFixture.h"
//----------------------------------------------------------------------------------------------------------------------
#include "embedded/test_embedding.nut.h"
//----------------------------------------------------------------------------------------------------------------------

typedef EmbeddingFixture TestEmbedding;

//----------------------------------------------------------------------------------------------------------------------
// TestEmbedding
//----------------------------------------------------------------------------------------------------------------------
SQTEST_EMBEDDED_TEST_CASE(test_embedding_nut);

//----------------------------------------------------------------------------------------------------------------------
TEST_F(TestEmbedding, EmptyScriptTest)
{
  // arrange and act
  //
  const SQChar *expectedFileContents = GetFileContents("scripts/test_embedding.nut");

  // assert
  //
  ASSERT_TRUE(test_embedding_nut != nullptr);
  EXPECT_STREQ(expectedFileContents, test_embedding_nut);
}

//----------------------------------------------------------------------------------------------------------------------
TEST_F(TestEmbedding, CheckEmbeddedTestWasRegistered)
{
  // Arrange and Act was done in the line SQTEST_EMBEDDED_TEST(test_embed_nut) above

  // Assert
  //
  ASSERT_TRUE(TestExists("test_embedding_nut", "embedded_test"));
}
