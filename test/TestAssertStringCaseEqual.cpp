//----------------------------------------------------------------------------------------------------------------------
// Copyright (c) 2013 James Whitworth
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//----------------------------------------------------------------------------------------------------------------------
#include "fixtures/SquirrelTestFixture.h"
//----------------------------------------------------------------------------------------------------------------------
#include <gtest/gtest-spi.h>
//----------------------------------------------------------------------------------------------------------------------
#include <sqtest/sqtest.h>
//----------------------------------------------------------------------------------------------------------------------

typedef SquirrelTestFixture TestAssertStringCaseEqual;

//----------------------------------------------------------------------------------------------------------------------
// TestAssertStringCaseEqual
//----------------------------------------------------------------------------------------------------------------------
TEST_F(TestAssertStringCaseEqual, PassTest)
{
  // arrange
  //
  sq_pushroottable(m_vm);

  auto original = GenerateString();
  sq_pushstring(m_vm, original, -1);
  auto other = GenerateDifferentCaseString(original);
  sq_pushstring(m_vm, other, -1);

  // act
  //
  auto result = sqtest_assert_strcaseeq(m_vm);

  // assert
  //
  EXPECT_EQ(0, result);
}

//----------------------------------------------------------------------------------------------------------------------
void CallAssertStrCaseEqWithDifferentValues()
{
  // arrange
  //
  auto vm = sq_open(1024);

  sq_pushroottable(vm);
  
  auto original = TestAssertStringCaseEqual::GenerateString();
  sq_pushstring(vm, original, -1);
  auto other = TestAssertStringCaseEqual::GenerateDifferentString(original);
  sq_pushstring(vm, other, -1);

  // act and assert
  //
  sqtest_assert_strcaseeq(vm);

  // cleanup
  //
  sq_close(vm);
}

//----------------------------------------------------------------------------------------------------------------------
TEST_F(TestAssertStringCaseEqual, FailTest)
{
  // have to do this with a static function for EXPECT_FATAL_FAILURE to work
  //
  EXPECT_FATAL_FAILURE(CallAssertStrCaseEqWithDifferentValues(), "");
}

//----------------------------------------------------------------------------------------------------------------------
TEST_F(TestAssertStringCaseEqual, PassScriptTest)
{
  // arrange
  //
  auto buffer = _SC("assert_strcaseeq(\"string\", \"STRING\")");

  // act and assert
  //
  EXPECT_NO_FATAL_FAILURE(CompileAndExecuteBuffer(buffer));
}

//----------------------------------------------------------------------------------------------------------------------
TEST_F(TestAssertStringCaseEqual, FailScriptTest)
{
  // arrange
  //
  static auto buffer = _SC("assert_strcaseeq(\"string\", \"other\")");

  // act and assert
  //
  EXPECT_FATAL_FAILURE(CompileAndExecuteBuffer(buffer), "");
}
