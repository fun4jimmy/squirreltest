//----------------------------------------------------------------------------------------------------------------------
// Copyright (c) 2013 James Whitworth
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//----------------------------------------------------------------------------------------------------------------------
#include "fixtures/SquirrelTestFixture.h"
//----------------------------------------------------------------------------------------------------------------------
#include <gtest/gtest-spi.h>
//----------------------------------------------------------------------------------------------------------------------
#include <sqtest/sqtest.h>
//----------------------------------------------------------------------------------------------------------------------

typedef SquirrelTestFixture TestAssertStringNotEqual;

//----------------------------------------------------------------------------------------------------------------------
// TestAssertStringNotEqual
//----------------------------------------------------------------------------------------------------------------------
TEST_F(TestAssertStringNotEqual, PassTest)
{
  // arrange
  //
  sq_pushroottable(m_vm);

  auto original = GenerateString();
  sq_pushstring(m_vm, original, -1);
  auto other = GenerateDifferentString(original);
  sq_pushstring(m_vm, other, -1);

  // act
  //
  auto result = sqtest_assert_strne(m_vm);

  // expect
  //
  EXPECT_EQ(0, result);
}

//----------------------------------------------------------------------------------------------------------------------
void CallAssertStrNeWithSameValue()
{
  // arrange
  //
  auto vm = sq_open(1024);

  sq_pushroottable(vm);
  
  auto value = TestAssertStringNotEqual::GenerateString();
  sq_pushstring(vm, value, -1);
  sq_pushstring(vm, value, -1);

  // act and expect
  //
  sqtest_assert_strne(vm);

  // cleanup
  //
  sq_close(vm);
}

//----------------------------------------------------------------------------------------------------------------------
TEST_F(TestAssertStringNotEqual, FailTest)
{
  // have to do this with a static function for EXPECT_FATAL_FAILURE to work
  //
  EXPECT_FATAL_FAILURE(CallAssertStrNeWithSameValue(), "");
}

//----------------------------------------------------------------------------------------------------------------------
TEST_F(TestAssertStringNotEqual, PassScriptTest)
{
  // arrange
  //
  auto buffer = _SC("assert_strne(\"original\", \"other\")");

  // act and expect
  //
  EXPECT_NO_FATAL_FAILURE(CompileAndExecuteBuffer(buffer));
}

//----------------------------------------------------------------------------------------------------------------------
TEST_F(TestAssertStringNotEqual, FailScriptTest)
{
  // arrange
  //
  static auto buffer = _SC("assert_strne(\"string\", \"string\")");

  // act and expect
  //
  EXPECT_FATAL_FAILURE(CompileAndExecuteBuffer(buffer), "");
}
