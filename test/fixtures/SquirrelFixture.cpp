//----------------------------------------------------------------------------------------------------------------------
// Copyright (c) 2013 James Whitworth
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//----------------------------------------------------------------------------------------------------------------------
#include "SquirrelFixture.h"
//----------------------------------------------------------------------------------------------------------------------
#include <sqstdblob.h>
#include <sqstdio.h>
#include <sqstdsystem.h>
#include <sqstdmath.h>
#include <sqstdstring.h>
//----------------------------------------------------------------------------------------------------------------------

//----------------------------------------------------------------------------------------------------------------------
// SquirrelFixture
//----------------------------------------------------------------------------------------------------------------------
SquirrelFixture::StringList SquirrelFixture::m_strings;

//----------------------------------------------------------------------------------------------------------------------
SQInteger SquirrelFixture::GenerateInteger()
{
  const SQInteger result = static_cast<SQInteger>(rand());
  return result;
}

//----------------------------------------------------------------------------------------------------------------------
SQInteger SquirrelFixture::GenerateDifferentInteger(const SQInteger original)
{
  SQInteger value;
  do 
  {
    value = GenerateInteger();
  }
  while (value == original);

  return value;
}

//----------------------------------------------------------------------------------------------------------------------
SQInteger SquirrelFixture::GenerateSmallerInteger(const SQInteger original)
{
  if (original == std::numeric_limits<SQInteger>::min())
  {
    ADD_FAILURE() << "Cannot generate an integer smaller than " << original;
    return original;
  }

  SQInteger value;
  do 
  {
    value = GenerateInteger();
  }
  while (value >= original);

  return value;
}

//----------------------------------------------------------------------------------------------------------------------
SQInteger SquirrelFixture::GenerateLargerInteger(const SQInteger original)
{
  if (original == std::numeric_limits<SQInteger>::max())
  {
    ADD_FAILURE() << "Cannot generate an integer larger than " << original;
    return original;
  }

  SQInteger value;
  do 
  {
    value = GenerateInteger();
  }
  while (value <= original);

  return value;
}

//----------------------------------------------------------------------------------------------------------------------
const SQChar *SquirrelFixture::GenerateString()
{
  const unsigned char minimum = 32; // space
  const unsigned char maximum = 126; // ~
  const unsigned char range = maximum - minimum;

  m_strings.push_back(std::basic_string<SQChar>());
  std::basic_string<SQChar> &string = m_strings.back();

  const size_t length = rand() % 32;
  string.resize(length);

  for (size_t i = 0; i != length; ++i)
  {
    const SQChar character = (rand() % range) + minimum;
    string[i] = character;
  }

  return string.c_str();
}

//----------------------------------------------------------------------------------------------------------------------
const SQChar *SquirrelFixture::GenerateDifferentString(const SQChar *original)
{
  const SQChar *other;
  do 
  {
    other = GenerateString();
  } while (scstrcmp(original, other) == 0);

  return other;
}

//----------------------------------------------------------------------------------------------------------------------
const SQChar *SquirrelFixture::GenerateDifferentCaseString(const SQChar *original)
{
  const unsigned char upperCaseLetterMinimum = 'A';
  const unsigned char upperCaseLetterMaximum = 'Z';
  const unsigned char lowerCaseLetterMinimum = 'a';
  const unsigned char lowerCaseLetterMaximum = 'z';

  m_strings.push_back(std::basic_string<SQChar>());
  std::basic_string<SQChar> &other = m_strings.back();

  const size_t length = scstrlen(original);
  other.resize(length);

  for (size_t i = 0; i != length; ++i)
  {
    const SQChar character = original[i];
    if (character >= upperCaseLetterMinimum && character <= upperCaseLetterMaximum)
    {
      other[i] = character + (lowerCaseLetterMinimum - upperCaseLetterMinimum);
    }
    else if (character >= lowerCaseLetterMinimum && character <= lowerCaseLetterMaximum)
    {
      other[i] = character - (lowerCaseLetterMinimum - upperCaseLetterMinimum);
    }
    else
    {
      other[i] = character;
    }
  }

  return other.c_str();
}

//----------------------------------------------------------------------------------------------------------------------
SQFloat SquirrelFixture::GenerateFloat()
{
  const SQFloat random = static_cast<SQFloat>(rand());
  const SQFloat scale = random / static_cast<SQFloat>(RAND_MAX);
  const SQFloat result = scale * std::numeric_limits<SQFloat>::max();
  return result;
}

//----------------------------------------------------------------------------------------------------------------------
SQFloat SquirrelFixture::GenerateDifferentFloat(const SQFloat original)
{
  const float error = original * 0.001f;
  const float minimum = original - error;
  const float maximum = original + error;

  SQFloat value;
  do 
  {
    value = GenerateFloat();
  }
  while (minimum <= value && value <= maximum);

  return value;
}

//----------------------------------------------------------------------------------------------------------------------
bool SquirrelFixture::CheckFunctionExists(HSQUIRRELVM vm, const SQChar *function)
{
  bool exists = false;

  sq_pushroottable(vm);

  sq_pushstring(vm, function, -1);
  if (SQ_SUCCEEDED(sq_rawget(vm, -2)))
  {
    const SQObjectType type = sq_gettype(vm, -1);
    exists = (type == OT_NATIVECLOSURE || type == OT_CLOSURE);
    sq_poptop(vm);
  }

  sq_poptop(vm);

  return exists;
}

//----------------------------------------------------------------------------------------------------------------------
SquirrelFixture::SquirrelFixture()
: m_vm(0)
{
}

//----------------------------------------------------------------------------------------------------------------------
SquirrelFixture::~SquirrelFixture()
{
}

//----------------------------------------------------------------------------------------------------------------------
void SquirrelFixture::SetUp()
{
  m_vm = sq_open(1024);

  sq_pushroottable(m_vm);

  sqstd_register_bloblib(m_vm);
  sqstd_register_iolib(m_vm);
  sqstd_register_systemlib(m_vm);
  sqstd_register_mathlib(m_vm);
  sqstd_register_stringlib(m_vm);

  sq_poptop(m_vm);
}

//----------------------------------------------------------------------------------------------------------------------
void SquirrelFixture::TearDown()
{
  sq_close(m_vm);
}
