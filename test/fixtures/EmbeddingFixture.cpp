//----------------------------------------------------------------------------------------------------------------------
// Copyright (c) 2014 James Whitworth
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//----------------------------------------------------------------------------------------------------------------------
#include "EmbeddingFixture.h"
//----------------------------------------------------------------------------------------------------------------------

namespace
{
std::basic_string<SQChar> WideStringFromUTF8(const char *utf8String)
{
  std::mbstate_t state;
  size_t wideStringLength = 0;
#if defined(_MSC_VER)
  mbsrtowcs_s(&wideStringLength, nullptr, 0, &utf8String, _TRUNCATE, &state);
#else
  wideStringLength = std::mbsrtowcs(nullptr, &utf8String, 0, &state);
#endif

  std::wstring wideString;
  wideString.resize(wideStringLength);

#if defined(_MSC_VER)
  mbsrtowcs_s(nullptr, &wideString[ 0 ], wideStringLength + 1, &utf8String, _TRUNCATE, &state);
#else
  std::mbsrtowcs(&wideString[ 0 ], &utf8String, wideStringLength + 1, &state);
#endif

  return wideString;
}

std::basic_string<SQChar> LoadFileContents(const std::string &fileName)
{
  // opening as ascii avoids annoying line ending differences on windows,
  // '/r/n' is converted to '/n' by fread
  //
  const char *fileMode = "r";

  // try to open the file as is
  //
#if defined(_MSC_VER)
  FILE *file = nullptr;
  fopen_s(&file, fileName.c_str(), fileMode);
#else
  FILE *file = fopen(fileName.c_str(), fileMode);
#endif

  if (file == nullptr)
  {
    // open the file assuming it is in the test directory and the current working directory 
    // is the project location (ie. squirreltest/build/vs2013)
    //
    std::string otherFileName = "../../test/";
    otherFileName.append(fileName);
#if defined(_MSC_VER)
    fopen_s(&file, otherFileName.c_str(), fileMode);
#else
    file = fopen(otherFileName.c_str(), fileMode);
#endif
  }

  if (file == nullptr)
  {
    // open the file assuming it is in the test directory and the current working directory
    // is the target location (ie. squirreltest/bin/vs2013/Win32-x86_32/debug)
    //
    std::string otherFileName = "../../../../test/";
    otherFileName.append(fileName);
#if defined(_MSC_VER)
    fopen_s(&file, otherFileName.c_str(), fileMode);
#else
    file = fopen(otherFileName.c_str(), fileMode);
#endif
  }

  EXPECT_TRUE(file != nullptr);
  if (file == nullptr)
  {
    return std::basic_string<SQChar>{};
  }

  std::string asciiContents;

  fseek(file, 0, SEEK_END);
  const size_t fileSize = ftell(file);
  fseek(file, 0, SEEK_SET);

  asciiContents.resize(fileSize);

  const size_t bytesRead = fread(&asciiContents[0], sizeof(char), fileSize, file);
  EXPECT_GT(bytesRead, 0u);
  // because we opened the file as ascii, bytes read can be less than file size
  //
  EXPECT_LE(bytesRead, fileSize);

  // trim the unused portion from asciiContents end
  //
  asciiContents.resize(bytesRead);

  fclose(file);

#if defined(SQUNICODE)
  return WideStringFromUTF8(asciiContents.c_str());
#else
  return asciiContents;
#endif
}

}

//----------------------------------------------------------------------------------------------------------------------
// EmbeddingFixture
//----------------------------------------------------------------------------------------------------------------------
EmbeddingFixture::FileContentsMap EmbeddingFixture::m_fileNameContentsLookup;

//----------------------------------------------------------------------------------------------------------------------
const SQChar *EmbeddingFixture::GetFileContents(std::string fileName)
{
  auto it = m_fileNameContentsLookup.find(fileName);
  if (it == m_fileNameContentsLookup.end())
  {
    auto contents = LoadFileContents(fileName);
    auto result = m_fileNameContentsLookup.emplace(std::move(fileName), std::move(contents));
    it = result.first;
  }
  const auto &contents = it->second;
  return contents.c_str();
}

//----------------------------------------------------------------------------------------------------------------------
bool EmbeddingFixture::TestExists(const char *testCaseName, const char *testName) const
{
  auto testInfo = FindTest(testCaseName, testName);
  return testInfo != nullptr;
}

//----------------------------------------------------------------------------------------------------------------------
bool EmbeddingFixture::TestPassed(const char *testCaseName, const char *testName) const
{
  auto testInfo = FindTest(testCaseName, testName);
  if (testInfo == nullptr)
  {
    ADD_FAILURE() << "test " << testCaseName << "/" << testName << " does not exist.";
    return false;
  }
  
  return testInfo->result()->Passed();
}

//----------------------------------------------------------------------------------------------------------------------
bool EmbeddingFixture::TestFailed(const char *testCaseName, const char *testName) const
{
  auto testInfo = FindTest(testCaseName, testName);
  if (testInfo == nullptr)
  {
    ADD_FAILURE() << "test " << testCaseName << "/" << testName << " does not exist.";
    return false;
  }

  return testInfo->result()->Failed();
}

//----------------------------------------------------------------------------------------------------------------------
EmbeddingFixture::EmbeddingFixture()
{
}

//----------------------------------------------------------------------------------------------------------------------
EmbeddingFixture::~EmbeddingFixture()
{
}

//----------------------------------------------------------------------------------------------------------------------
const testing::TestInfo *EmbeddingFixture::FindTest(const char *testCaseName, const char *testName) const
{
  const auto unitTest = testing::UnitTest::GetInstance();

  for (int i = 0; i < unitTest->total_test_case_count(); ++i)
  {
    const auto testCase = unitTest->GetTestCase(i);
    if (strcmp(testCase->name(), testCaseName) == 0)
    {
      for (int j = 0; j != testCase->total_test_count(); ++j)
      {
        const auto test_info = testCase->GetTestInfo(j);
        if (strcmp(test_info->name(), testName) == 0)
        {
          return test_info;
        }
      }
    }
  }

  return nullptr;
}
