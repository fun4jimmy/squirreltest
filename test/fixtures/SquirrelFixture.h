#pragma once
//----------------------------------------------------------------------------------------------------------------------
// Copyright (c) 2013 James Whitworth
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//----------------------------------------------------------------------------------------------------------------------
#include <list>
#include <string>
//----------------------------------------------------------------------------------------------------------------------
#include <gtest/gtest.h>
//----------------------------------------------------------------------------------------------------------------------
#include <squirrel.h>
//----------------------------------------------------------------------------------------------------------------------

//----------------------------------------------------------------------------------------------------------------------
/// \brief
//----------------------------------------------------------------------------------------------------------------------
class SquirrelFixture : public testing::Test
{
public:
  static SQInteger GenerateInteger();
  static SQInteger GenerateDifferentInteger(const SQInteger original);
  static SQInteger GenerateSmallerInteger(const SQInteger original);
  static SQInteger GenerateLargerInteger(const SQInteger original);

  static const SQChar *GenerateString();
  static const SQChar *GenerateDifferentString(const SQChar *original);
  /// \brief Generates a string with the same letters as the original but different case.
  static const SQChar *GenerateDifferentCaseString(const SQChar *original);

  static SQFloat GenerateFloat();
  static SQFloat GenerateDifferentFloat(const SQFloat original);

  static bool CheckFunctionExists(HSQUIRRELVM vm, const SQChar *function);

protected:
  HSQUIRRELVM m_vm;

  SquirrelFixture();
  ~SquirrelFixture();

  virtual void SetUp();
  virtual void TearDown();

private:
  // has to be a list so insertion doesn't invalidate the existing entries
  //
  typedef std::list< std::basic_string<SQChar> > StringList;
  static StringList m_strings;
};
