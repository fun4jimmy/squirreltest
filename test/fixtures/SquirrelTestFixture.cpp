//----------------------------------------------------------------------------------------------------------------------
// Copyright (c) 2013 James Whitworth
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//----------------------------------------------------------------------------------------------------------------------
#include "SquirrelTestFixture.h"
//----------------------------------------------------------------------------------------------------------------------
#include <sqstdblob.h>
#include <sqstdio.h>
#include <sqstdsystem.h>
#include <sqstdmath.h>
#include <sqstdstring.h>
//----------------------------------------------------------------------------------------------------------------------
#include <sqtest/sqtest.h>
//----------------------------------------------------------------------------------------------------------------------

//----------------------------------------------------------------------------------------------------------------------
// SquirrelTestFixture
//----------------------------------------------------------------------------------------------------------------------
SquirrelTestFixture::SquirrelTestFixture()
{
}

//----------------------------------------------------------------------------------------------------------------------
SquirrelTestFixture::~SquirrelTestFixture()
{
}

//----------------------------------------------------------------------------------------------------------------------
void SquirrelTestFixture::CompileAndExecuteBuffer(const SQChar *buffer)
{
  HSQUIRRELVM vm = sq_open(1024);

  sq_pushroottable(vm);

  sqstd_register_bloblib(vm);
  sqstd_register_iolib(vm);
  sqstd_register_systemlib(vm);
  sqstd_register_mathlib(vm);
  sqstd_register_stringlib(vm);

  sqtest_register_lib(vm);

  sq_poptop(vm);

  const size_t length = scstrlen(buffer);
  SQRESULT result = sq_compilebuffer(vm, buffer, length, _SC("buffer"), SQFalse);
  ASSERT_TRUE(SQ_SUCCEEDED(result));

  sq_pushroottable(vm);
  // if the buffer contains an assert_* check it will also throw an error so don't assert the result
  //
  result = sq_call(vm, 1, SQFalse, SQFalse);

  sq_close(vm);
}
