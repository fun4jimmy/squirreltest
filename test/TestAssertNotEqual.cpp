//----------------------------------------------------------------------------------------------------------------------
// Copyright (c) 2013 James Whitworth
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//----------------------------------------------------------------------------------------------------------------------
#include "fixtures/SquirrelTestFixture.h"
//----------------------------------------------------------------------------------------------------------------------
#include <gtest/gtest-spi.h>
//----------------------------------------------------------------------------------------------------------------------
#include <sqtest/sqtest.h>
//----------------------------------------------------------------------------------------------------------------------

typedef SquirrelTestFixture TestAssertNotEqual;

//----------------------------------------------------------------------------------------------------------------------
// TestAssertNotEqual
//----------------------------------------------------------------------------------------------------------------------
TEST_F(TestAssertNotEqual, PassTest)
{
  // arrange
  //
  sq_pushroottable(m_vm);

  const auto original = GenerateInteger();
  sq_pushinteger(m_vm, original);
  const auto other = GenerateDifferentInteger(original);
  sq_pushinteger(m_vm, other);

  // act
  //
  auto result = sqtest_assert_ne(m_vm);

  // assert
  //
  EXPECT_EQ(0, result);
}

//----------------------------------------------------------------------------------------------------------------------
void CallAssertNeWithSameValue()
{
  // arrange
  //
  auto vm = sq_open(1024);

  sq_pushroottable(vm);
  
  const auto value = TestAssertNotEqual::GenerateInteger();
  sq_pushinteger(vm, value);
  sq_pushinteger(vm, value);

  // act and assert
  //
  sqtest_assert_ne(vm);

  // cleanup
  //
  sq_close(vm);
}

//----------------------------------------------------------------------------------------------------------------------
TEST_F(TestAssertNotEqual, FailTest)
{
  // have to do this with a static function for EXPECT_FATAL_FAILURE to work
  //
  EXPECT_FATAL_FAILURE(CallAssertNeWithSameValue(), "");
}

//----------------------------------------------------------------------------------------------------------------------
TEST_F(TestAssertNotEqual, PassScriptTest)
{
  // arrange
  //
  auto buffer = _SC("assert_ne(10, 20)");

  // act and assert
  //
  EXPECT_NO_FATAL_FAILURE(CompileAndExecuteBuffer(buffer));
}

//----------------------------------------------------------------------------------------------------------------------
TEST_F(TestAssertNotEqual, FailScriptTest)
{
  // arrange
  //
  static auto buffer = _SC("assert_ne(10, 10)");

  // act and assert
  //
  EXPECT_FATAL_FAILURE(CompileAndExecuteBuffer(buffer), "");
}
