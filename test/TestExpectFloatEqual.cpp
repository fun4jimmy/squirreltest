//----------------------------------------------------------------------------------------------------------------------
// Copyright (c) 2013 James Whitworth
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//----------------------------------------------------------------------------------------------------------------------
#include "fixtures/SquirrelTestFixture.h"
//----------------------------------------------------------------------------------------------------------------------
#include <gtest/gtest-spi.h>
//----------------------------------------------------------------------------------------------------------------------
#include <sqtest/sqtest.h>
//----------------------------------------------------------------------------------------------------------------------

typedef SquirrelTestFixture TestExpectFloatEqual;

//----------------------------------------------------------------------------------------------------------------------
// TestExpectFloatEqual
//----------------------------------------------------------------------------------------------------------------------
TEST_F(TestExpectFloatEqual, PassTest)
{
  // arrange
  //
  sq_pushroottable(m_vm);

  const auto value = GenerateFloat();
  sq_pushfloat(m_vm, value);
  sq_pushfloat(m_vm, value);

  // act
  //
  auto result = sqtest_expect_float_eq(m_vm);

  // assert
  //
  EXPECT_EQ(0, result);
}

//----------------------------------------------------------------------------------------------------------------------
void CallExpectFloatEqWithDifferentValues()
{
  // arrange
  //
  auto vm = sq_open(1024);

  sq_pushroottable(vm);
  
  const auto original = TestExpectFloatEqual::GenerateFloat();
  sq_pushfloat(vm, original);
  const auto other = TestExpectFloatEqual::GenerateDifferentFloat(original);
  sq_pushfloat(vm, other);

  // act and assert
  //
  sqtest_expect_float_eq(vm);

  // cleanup
  //
  sq_close(vm);
}

//----------------------------------------------------------------------------------------------------------------------
TEST_F(TestExpectFloatEqual, FailTest)
{
  // have to do this with a static function for EXPECT_NONFATAL_FAILURE to work
  //
  EXPECT_NONFATAL_FAILURE(CallExpectFloatEqWithDifferentValues(), "");
}

//----------------------------------------------------------------------------------------------------------------------
TEST_F(TestExpectFloatEqual, PassScriptTest)
{
  // arrange
  //
  auto buffer = _SC("expect_float_eq(0.1, 0.1)");

  // act and assert
  //
  EXPECT_NO_FATAL_FAILURE(CompileAndExecuteBuffer(buffer));
}

//----------------------------------------------------------------------------------------------------------------------
TEST_F(TestExpectFloatEqual, FailScriptTest)
{
  // arrange
  //
  static auto buffer = _SC("expect_float_eq(10.0, 20.0)");

  // act and assert
  //
  EXPECT_NONFATAL_FAILURE(CompileAndExecuteBuffer(buffer), "");
}
