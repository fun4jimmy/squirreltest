//----------------------------------------------------------------------------------------------------------------------
// Copyright (c) 2013 James Whitworth
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//----------------------------------------------------------------------------------------------------------------------
#include "fixtures/SquirrelTestFixture.h"
//----------------------------------------------------------------------------------------------------------------------
#include <gtest/gtest-spi.h>
//----------------------------------------------------------------------------------------------------------------------
#include <sqtest/sqtest.h>
//----------------------------------------------------------------------------------------------------------------------

typedef SquirrelTestFixture TestExpectStringEqual;

//----------------------------------------------------------------------------------------------------------------------
// TestExpectStringEqual
//----------------------------------------------------------------------------------------------------------------------
TEST_F(TestExpectStringEqual, PassTest)
{
  // arrange
  //
  sq_pushroottable(m_vm);

  auto value = GenerateString();
  sq_pushstring(m_vm, value, -1);
  sq_pushstring(m_vm, value, -1);

  // act
  //
  auto result = sqtest_expect_streq(m_vm);

  // expect
  //
  EXPECT_EQ(0, result);
}

//----------------------------------------------------------------------------------------------------------------------
void CallExpectStrEqWithDifferentValues()
{
  // arrange
  //
  auto vm = sq_open(1024);

  sq_pushroottable(vm);
  
  auto original = TestExpectStringEqual::GenerateString();
  sq_pushstring(vm, original, -1);
  auto other = TestExpectStringEqual::GenerateDifferentString(original);
  sq_pushstring(vm, other, -1);

  // act and expect
  //
  sqtest_expect_streq(vm);

  // cleanup
  //
  sq_close(vm);
}

//----------------------------------------------------------------------------------------------------------------------
TEST_F(TestExpectStringEqual, FailTest)
{
  // have to do this with a static function for EXPECT_NONFATAL_FAILURE to work
  //
  EXPECT_NONFATAL_FAILURE(CallExpectStrEqWithDifferentValues(), "");
}

//----------------------------------------------------------------------------------------------------------------------
TEST_F(TestExpectStringEqual, PassScriptTest)
{
  // arrange
  //
  auto buffer = _SC("expect_streq(\"string\", \"string\")");

  // act and expect
  //
  EXPECT_NO_FATAL_FAILURE(CompileAndExecuteBuffer(buffer));
}

//----------------------------------------------------------------------------------------------------------------------
TEST_F(TestExpectStringEqual, FailScriptTest)
{
  // arrange
  //
  static auto buffer = _SC("expect_streq(\"string\", \"other\")");

  // act and expect
  //
  EXPECT_NONFATAL_FAILURE(CompileAndExecuteBuffer(buffer), "");
}
