//----------------------------------------------------------------------------------------------------------------------
// Copyright (c) 2013 James Whitworth
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//----------------------------------------------------------------------------------------------------------------------
#include <sqtest/sqtScriptRegistryTableHelper.h>
//----------------------------------------------------------------------------------------------------------------------

namespace sqt
{
//----------------------------------------------------------------------------------------------------------------------
// ScriptRegistryTableHelper
//----------------------------------------------------------------------------------------------------------------------
std::shared_ptr<const Script> ScriptRegistryTableHelper::GetScript(HSQUIRRELVM vm, const SQChar *key)
{
  sq_pushregistrytable(vm);
  sq_pushstring(vm, key, -1);
  if (SQ_FAILED(sq_rawget(vm, -2)))
  {
    return std::shared_ptr<const Script>{};
  }

  if (sq_gettype(vm, -1) != OT_USERPOINTER)
  {
    return std::shared_ptr<const Script>{};
  }

  SQUserPointer pointer = nullptr;
  sq_getuserpointer(vm, -1, &pointer);

  auto script = *static_cast<const std::shared_ptr<const Script> *>(pointer);
  return script;
}

//----------------------------------------------------------------------------------------------------------------------
ScriptRegistryTableHelper::ScriptRegistryTableHelper(HSQUIRRELVM vm,
                                                     std::shared_ptr<const Script> script,
                                                     std::basic_string<SQChar> key)
: m_key(std::move(key))
, m_script(script)
, m_vm(vm)
{
  sq_pushregistrytable(m_vm);
  sq_pushstring(m_vm, m_key.c_str(), m_key.size());
  sq_pushuserpointer(m_vm, const_cast<std::shared_ptr<const Script> *>(&m_script));
  sq_rawset(m_vm, -3);
  sq_poptop(m_vm);
}

//----------------------------------------------------------------------------------------------------------------------
// RemoveTestScriptFromRegistryTable
//----------------------------------------------------------------------------------------------------------------------
ScriptRegistryTableHelper::~ScriptRegistryTableHelper()
{
  sq_pushstring(m_vm, m_key.c_str(), m_key.size());
  sq_rawdeleteslot(m_vm, -2, SQFalse);
  sq_poptop(m_vm);
}

} // namespace sqt
