//----------------------------------------------------------------------------------------------------------------------
// Copyright (c) 2014 James Whitworth
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//----------------------------------------------------------------------------------------------------------------------
#include <sqtest/sqtTest.h>
//----------------------------------------------------------------------------------------------------------------------
#include <squirrel.h>
//----------------------------------------------------------------------------------------------------------------------
#include <sqstdaux.h>
#include <sqstdblob.h>
#include <sqstdio.h>
#include <sqstdmath.h>
#include <sqstdstring.h>
#include <sqstdsystem.h>
//----------------------------------------------------------------------------------------------------------------------
#include <sqtest/sqtest.h>
//----------------------------------------------------------------------------------------------------------------------

namespace sqt
{
//----------------------------------------------------------------------------------------------------------------------
// Test
//----------------------------------------------------------------------------------------------------------------------
Test::Test(std::shared_ptr<const Script> script)
: TestBase(script)
, m_vm(sq_open(1024))
{
  if (m_vm == nullptr)
  {
    ADD_FAILURE() << "sq_open() failed.";
    return;
  }

  sq_pushroottable(m_vm);

  if (SQ_FAILED(sqstd_register_bloblib(m_vm)))
  {
    ADD_FAILURE() << "sqstd_register_bloblib() failed.";
  }

  if (SQ_FAILED(sqstd_register_iolib(m_vm)))
  {
    ADD_FAILURE() << "sqstd_register_iolib() failed.";
  }

  if (SQ_FAILED(sqstd_register_systemlib(m_vm)))
  {
    ADD_FAILURE() << "sqstd_register_systemlib() failed.";
  }

  if (SQ_FAILED(sqstd_register_mathlib(m_vm)))
  {
    ADD_FAILURE() << "sqstd_register_mathlib() failed.";
  }

  if (SQ_FAILED(sqstd_register_stringlib(m_vm)))
  {
    ADD_FAILURE() << "sqstd_register_stringlib() failed.";
  }

  if (SQ_FAILED(sqtest_register_lib(m_vm)))
  {
    ADD_FAILURE() << "sqtest_register_lib() failed.";
  }

  // remove the root table
  //
  sq_poptop(m_vm);
}

//----------------------------------------------------------------------------------------------------------------------
Test::~Test()
{
  sq_close(m_vm);
}

//----------------------------------------------------------------------------------------------------------------------
HSQUIRRELVM Test::GetVM() const
{
  return m_vm;
}

//----------------------------------------------------------------------------------------------------------------------
// explicit template instantiation
//----------------------------------------------------------------------------------------------------------------------
template class TestFactory<Test>;

} // namespace sqt
