//----------------------------------------------------------------------------------------------------------------------
// Copyright (c) 2013 James Whitworth
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//----------------------------------------------------------------------------------------------------------------------
#include <sqtest/sqtTestBase.h>
//----------------------------------------------------------------------------------------------------------------------
#include <cwchar>
//----------------------------------------------------------------------------------------------------------------------
#include <sqtest/sqtScript.h>
#include <sqtest/sqtScriptRegistryTableHelper.h>
//----------------------------------------------------------------------------------------------------------------------

namespace
{
std::wstring WideStringFromUTF8(const char *utf8String)
{
  std::mbstate_t state;
  size_t wideStringLength = 0;
#if defined(_MSC_VER)
  mbsrtowcs_s(&wideStringLength, nullptr, 0, &utf8String, _TRUNCATE, &state);
#else
  wideStringLength = std::mbsrtowcs(nullptr, &utf8String, 0, &state);
#endif

  std::wstring wideString;
  wideString.resize(wideStringLength);

#if defined(_MSC_VER)
  mbsrtowcs_s(nullptr, &wideString[0], wideStringLength + 1, &utf8String, _TRUNCATE, &state);
#else
  std::mbsrtowcs(&wideString[0], &utf8String, wideStringLength + 1, &state);
#endif

  return wideString;
}

}

namespace sqt
{
//----------------------------------------------------------------------------------------------------------------------
// TestBase
//----------------------------------------------------------------------------------------------------------------------
const char *TestBase::kEmptyScriptTestName = "EmptyScript";

//----------------------------------------------------------------------------------------------------------------------
TestBase::TestBase(std::shared_ptr<const Script> script)
: m_script(std::move(script))
{
}

//----------------------------------------------------------------------------------------------------------------------
TestBase::~TestBase()
{
}

//----------------------------------------------------------------------------------------------------------------------
std::string TestBase::GetTestCaseNameUTF8() const
{
  const auto unitTest = testing::UnitTest::GetInstance();
  const auto currentTestCase = unitTest->current_test_case();
  const auto currentTestCaseName = currentTestCase->name();
  return std::string{ currentTestCaseName };
}

//----------------------------------------------------------------------------------------------------------------------
std::basic_string<SQChar> TestBase::GetTestCaseName() const
{
  auto testCaseNameUTF8 = GetTestCaseNameUTF8();
#if defined(SQUNICODE)
  return WideStringFromUTF8(testCaseNameUTF8.c_str());
#else
  return testCaseNameUTF8;
#endif
}

//----------------------------------------------------------------------------------------------------------------------
std::string TestBase::GetTestNameUTF8() const
{
  const auto unitTest = testing::UnitTest::GetInstance();
  const auto currentTestInfo = unitTest->current_test_info();
  const auto currentTestName = currentTestInfo->name();
  return std::string{ currentTestName };
}

//----------------------------------------------------------------------------------------------------------------------
std::basic_string<SQChar> TestBase::GetTestName() const
{
  auto testNameUTF8 = GetTestNameUTF8();
#if defined(SQUNICODE)
  return WideStringFromUTF8(testNameUTF8.c_str());
#else
  return testNameUTF8;
#endif
}

//----------------------------------------------------------------------------------------------------------------------
void TestBase::TestBody()
{
  const auto testCaseNameUTF8 = GetTestCaseNameUTF8();
  const auto testNameUTF8 = GetTestNameUTF8();

  ASSERT_STRNE(testNameUTF8.c_str(), kEmptyScriptTestName) << testCaseNameUTF8 << " contains no tests.";

  auto vm = GetVM();
  const auto testCaseName = GetTestCaseName();

  ScriptRegistryTableHelper helper{vm, m_script, testCaseName};

  ExecuteScript();

  CallTestFunction();
}

//----------------------------------------------------------------------------------------------------------------------
void TestBase::ExecuteScript()
{
  auto vm = GetVM();

  sq_pushroottable(vm);

  sqtest_register_lib(vm);

  sq_pushstring(vm, _SC("test"), 4);
  sq_newtable(vm);
  sq_rawset(vm, -3);

  sq_poptop(vm);

  size_t contentsLength = 0;
  const auto contents = m_script->GetContents(&contentsLength);

  const auto testCaseName = GetTestCaseName();
  auto result = sq_compilebuffer(vm, contents, contentsLength, testCaseName.c_str(), SQTrue);

  const auto testCaseNameUTF8 = GetTestCaseNameUTF8();
  ASSERT_TRUE(SQ_SUCCEEDED(result)) << "compiling script " << testCaseNameUTF8 << " failed.";

  sq_pushroottable(vm);
  result = sq_call(vm, 1, SQFalse, SQTrue);
  ASSERT_TRUE(SQ_SUCCEEDED(result)) << "executing script " << testCaseNameUTF8 << " failed.";;
  sq_poptop(vm);
}

//----------------------------------------------------------------------------------------------------------------------
void TestBase::CallTestFunction()
{
  const auto testName = GetTestName();
  const auto bufferLength = 5 + testName.size() + 2 + 1;

  auto buffer = std::basic_string<SQChar>{};
  buffer.reserve(bufferLength);

  buffer.append(_SC("test."));
  buffer.append(testName);
  buffer.append(_SC("()"));

  HSQUIRRELVM vm = GetVM();
  sq_compilebuffer(vm, buffer.c_str(), bufferLength, _SC(""), SQTrue);

  sq_pushroottable(vm);

  sq_call(vm, 1, SQFalse, SQTrue);

  sq_poptop(vm);
}

} // namespace sqt
