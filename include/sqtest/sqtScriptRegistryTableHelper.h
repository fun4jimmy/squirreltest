#pragma once
//----------------------------------------------------------------------------------------------------------------------
// Copyright (c) 2013 James Whitworth
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//----------------------------------------------------------------------------------------------------------------------
#include <memory>
#include <string>
//----------------------------------------------------------------------------------------------------------------------
#include <sqtest/sqtest.h>
//----------------------------------------------------------------------------------------------------------------------

namespace sqt
{
//----------------------------------------------------------------------------------------------------------------------
// forward declarations
//----------------------------------------------------------------------------------------------------------------------
class Script;
//----------------------------------------------------------------------------------------------------------------------

//----------------------------------------------------------------------------------------------------------------------
/// Scoped helper class that will add and remove a script to the registry table on construction and destruction.
//----------------------------------------------------------------------------------------------------------------------
class SQTEST_API ScriptRegistryTableHelper
{
public:
  /// Used by predicate functions to retrieve a script that has been added to the registry table.
  /// Will return a null pointer if no script with that key was found.
  static std::shared_ptr<const Script> GetScript(HSQUIRRELVM vm, const SQChar *key);

  /// This will add the script to the registry table of vm
  ScriptRegistryTableHelper(HSQUIRRELVM vm,
                            std::shared_ptr<const Script> script,
                            std::basic_string<SQChar> key);

  /// This will remove the script that was added.
  ~ScriptRegistryTableHelper();

private:
  const std::basic_string<SQChar> m_key;
  /// Keeping this as a member ensures that we hold a reference to it throughout the time it
  /// is accessible through the registry table.
  const std::shared_ptr<const Script> m_script;
  HSQUIRRELVM m_vm;

  // disable copy construction and assignment
  ScriptRegistryTableHelper(const ScriptRegistryTableHelper &) = delete;
  ScriptRegistryTableHelper &operator = (const ScriptRegistryTableHelper &) = delete;
};

} // namespace sqt
