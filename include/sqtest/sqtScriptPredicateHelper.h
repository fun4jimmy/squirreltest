#pragma once
//----------------------------------------------------------------------------------------------------------------------
// Copyright (c) 2013 James Whitworth
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//----------------------------------------------------------------------------------------------------------------------
#include <cstddef>
#include <string>
//----------------------------------------------------------------------------------------------------------------------
#include <sqtest/sqtest.h>
//----------------------------------------------------------------------------------------------------------------------

namespace sqt
{
//----------------------------------------------------------------------------------------------------------------------
// forward declarations
//----------------------------------------------------------------------------------------------------------------------
class SQTEST_API Script;
//----------------------------------------------------------------------------------------------------------------------

//----------------------------------------------------------------------------------------------------------------------
/// Used by squirrel test functions to find the text required to display predicate errors.
//----------------------------------------------------------------------------------------------------------------------
class SQTEST_API ScriptPredicateHelper
{
public:

  static std::string ConvertStringUtf8(const char *input);

  static std::string ConvertStringUtf8(const wchar_t *input);

  static bool GetPredicateText1(HSQUIRRELVM vm,
                                const Script &script,
                                const size_t line,
                                std::string *predicateText);

  static bool GetPredicateText2(HSQUIRRELVM vm,
                                const Script &script,
                                const size_t line,
                                std::string *firstPredicateText,
                                std::string *secondPredicateText);

private:
  /// Pushes the text of a predicate function that takes one argument, ie. expect_true(statement), on to vm's stack.
  static bool PushPredicateText1(HSQUIRRELVM vm, const Script &script, const size_t line);
  /// Pushes the text of a predicate function that takes two arguments, ie. expect_equal(expected, actual), on to
  /// vm's stack.
  static bool PushPredicateText2(HSQUIRRELVM vm, const Script &script, const size_t line);
};

} // namespace sqt
