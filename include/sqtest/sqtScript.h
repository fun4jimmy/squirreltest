#pragma once
//----------------------------------------------------------------------------------------------------------------------
// Copyright (c) 2013 James Whitworth
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//----------------------------------------------------------------------------------------------------------------------
#include <cstddef>
#include <memory>
//----------------------------------------------------------------------------------------------------------------------
#include <sqtest/sqtest.h>
//----------------------------------------------------------------------------------------------------------------------

namespace sqt
{
//----------------------------------------------------------------------------------------------------------------------
/// \brief
//----------------------------------------------------------------------------------------------------------------------
class SQTEST_API Script
{
public:
  enum ParseResult
  {
    kParseEmptyContents,
    kParseCompilationError,
    kParseExecutionError,
    kParseNoTestsFound,
    kParseSuccess,
  };

  Script();
  Script(const Script &rhs);
  Script(Script &&rhs);

  ~Script();

  Script &operator = (const Script &other);
  Script &operator = (Script&& other);

  /// \brief Clear the test contents, also clears the test info.
  void ClearContents();

  const SQChar *GetContents() const;
  const SQChar *GetContents(size_t *contentsLength) const;

  size_t GetLineCount() const;
  const SQChar *GetLine(size_t index) const;
  size_t GetLineLength(size_t index) const;

  void SetContents(const SQChar *contents);
  /// \brief Sets the contents of the test script by copying contents, contentsLength should be the length of the
  ///   string not including the terminating null character.
  void SetContents(const SQChar *contents, size_t contentsLength);

  /// \brief Sets the contents of the test script also taking responsibility for freeing the contents.
  /// \note Contents must be allocated with new as it will be deallocated with delete.
  void SetContentsTakeOwnership(SQChar *contents, size_t contentsLength);

  /// \brief Parse the contents of the script looking for any functions within the namespace test.
  ParseResult ParseContents();

  void ClearTestInfo();

  size_t GetTestCount() const;
  const SQChar *GetTestName(size_t index) const;

protected:
  SQChar   *m_contents;
  size_t    m_contentsLength;

  size_t   *m_lineOffsets;
  size_t    m_lineCount;

  SQChar  **m_testNames;
  size_t    m_testCount;

  void CalculateLineOffsets();
};

//----------------------------------------------------------------------------------------------------------------------
/// \brief Loads a test script from disk using fopen
//----------------------------------------------------------------------------------------------------------------------
SQTEST_API std::shared_ptr<Script> LoadScript(const char *fileName);

//----------------------------------------------------------------------------------------------------------------------
/// \brief Loads and parses a test script from disk using fopen.
//----------------------------------------------------------------------------------------------------------------------
SQTEST_API std::shared_ptr<Script> LoadScriptAndParseContents(const char *fileName);

} // namespace sqt
