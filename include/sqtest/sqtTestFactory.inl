#pragma once
//----------------------------------------------------------------------------------------------------------------------
// Copyright (c) 2014 James Whitworth
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//----------------------------------------------------------------------------------------------------------------------

namespace sqt
{
//----------------------------------------------------------------------------------------------------------------------
// TestFactory<TestType>
//----------------------------------------------------------------------------------------------------------------------
template<class TestType>
inline SQRESULT TestFactory<TestType>::RegisterTestCaseScript(const char *testCaseName,
                                                              std::shared_ptr<const Script> testCaseScript)
{
  const size_t testCount = testCaseScript->GetTestCount();
  if (testCount == 0)
  {
    testing::internal::MakeAndRegisterTestInfo(
      testCaseName,
      TestBase::kEmptyScriptTestName,
      NULL,
      NULL,
      ::testing::internal::GetTestTypeId(),
      TestType::SetUpTestCase,
      TestType::TearDownTestCase,
      new TestFactory<TestType>(testCaseScript));
    return SQ_ERROR;
  }
  else
  {
    for (size_t i = 0; i != testCount; ++i)
    {
      const SQChar *testName = testCaseScript->GetTestName(i);
      const auto testNameUtf8 = ScriptPredicateHelper::ConvertStringUtf8(testName);
      testing::internal::MakeAndRegisterTestInfo(
        testCaseName,
        testNameUtf8.c_str(),
        NULL,
        NULL,
        ::testing::internal::GetTestTypeId(),
        TestType::SetUpTestCase,
        TestType::TearDownTestCase,
        new TestFactory<TestType>(testCaseScript));
    }
    return SQ_OK;
  }
}

//----------------------------------------------------------------------------------------------------------------------
template<class TestType>
inline SQRESULT TestFactory<TestType>::RegisterTestCaseString(const char *testCaseName,
                                                              const SQChar *testCaseString)
{
  auto testCaseScript = [&] ()
  {
    sqt::Script script;
    script.SetContents(testCaseString);
    script.ParseContents();
    return std::make_shared<const sqt::Script>(std::move(script));
  }();

  if (testCaseScript)
  {
    return RegisterTestCaseScript(testCaseName, testCaseScript);
  }

  return SQ_ERROR;
}

//----------------------------------------------------------------------------------------------------------------------
template<class TestType>
inline TestFactory<TestType>::TestFactory(std::shared_ptr<const Script> script)
: m_script(std::move(script))
{
}

//----------------------------------------------------------------------------------------------------------------------
template<class TestType>
inline TestFactory<TestType>::~TestFactory()
{
}

//----------------------------------------------------------------------------------------------------------------------
template<class TestType>
inline testing::Test *TestFactory<TestType>::CreateTest()
{
  return new TestType(m_script);
}

} // namespace sqt
