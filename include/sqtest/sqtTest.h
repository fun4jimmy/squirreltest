#pragma once
//----------------------------------------------------------------------------------------------------------------------
// Copyright (c) 2014 James Whitworth
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//----------------------------------------------------------------------------------------------------------------------
#include <sqtest/sqtTestBase.h>
#include <sqtest/sqtTestFactory.h>
//----------------------------------------------------------------------------------------------------------------------

namespace sqt
{
//----------------------------------------------------------------------------------------------------------------------
/// Basic derived squirrel test fixture that provides a vm with all squirrel standard library functions registered.
/// Can be used to test scripts that do not depend on any user defined native functions.
/// This is the fixture used by SQTEST_EMBEDDED_TEST_CASE.
//----------------------------------------------------------------------------------------------------------------------
class Test : public TestBase
{
  friend class sqt::TestFactory<Test>;

protected:
  /// Opens m_vm and registers all standard library functions.
  /// \note We can't use SQTEST_IMPLEMENT_FORWARDING_CONSTRUCTOR as this constructor does more than just
  /// forward script to TestBase.
  Test(std::shared_ptr<const Script> script);

  /// Closes m_vm
  virtual ~Test();

  // TestBase override.
  virtual HSQUIRRELVM GetVM() const override;

private:
  HSQUIRRELVM m_vm;
};

/// Declare the specialisation of this template as extern to save other compilation units recompiling the template.
extern template class TestFactory<Test>;

} // namespace sqt
