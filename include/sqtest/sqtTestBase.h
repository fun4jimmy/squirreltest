#pragma once
//----------------------------------------------------------------------------------------------------------------------
// Copyright (c) 2013 James Whitworth
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//----------------------------------------------------------------------------------------------------------------------
#include <memory>
//----------------------------------------------------------------------------------------------------------------------
#include <squirrel.h>
//----------------------------------------------------------------------------------------------------------------------
#include <gtest/gtest.h>
//----------------------------------------------------------------------------------------------------------------------
#include <sqtest/sqtest.h>
//----------------------------------------------------------------------------------------------------------------------

namespace sqt
{
//----------------------------------------------------------------------------------------------------------------------
// forward declarations
//----------------------------------------------------------------------------------------------------------------------
class SQTEST_API Script;
//----------------------------------------------------------------------------------------------------------------------

//----------------------------------------------------------------------------------------------------------------------
/// Base fixture used for all squirrel tests, any custom fixture squirrel tests should be derived from this class.
//----------------------------------------------------------------------------------------------------------------------
class SQTEST_API TestBase : public testing::Test
{
public:
  /// This is the name that tests will be registered with if their Script is found to contain no tests.
  static const char *kEmptyScriptTestName;

protected:
  /// Any fixture deriving from TestBase must have a constructor matching this signature as sqt::TestFactory
  /// relies on passing the script parameter in as an argument.
  /// Use SQTEST_IMPLEMENT_FORWARDING_CONSTRUCTOR to save manually writing a forwarding constructor.
  TestBase(std::shared_ptr<const Script> script);

  virtual ~TestBase();

  /// The squirrel vm used to compile and execute the test code.
  virtual HSQUIRRELVM GetVM() const = 0;

protected:
  /// The squirrel script containing the test to be run.
  const std::shared_ptr<const Script> m_script;

  std::string GetTestCaseNameUTF8() const;
  std::basic_string<SQChar> GetTestCaseName() const;

  std::string GetTestNameUTF8() const;
  std::basic_string<SQChar> GetTestName() const;

private:
  // testing::Test override
  virtual void TestBody() final;

  /// Compiles and executes m_script. 
  /// \note Before compilation the squirrel test lib is registered and the table
  /// 'test' is added to the root table.
  void ExecuteScript();

  /// Calls the test function 'test.test_name()'.
  void CallTestFunction();
};

} // namespace sqt

/// Use this macro in fixtures derived from sqt::TestBase to save having to manually author
/// forwarding constructors to pass on the script parameter.
/// \code
/// class DerivedTest : public sqt::TestBase
/// {
/// public:
///   SQTEST_IMPLEMENT_FORWARDING_CONSTRUCTOR(DerivedTest, sqt::TestBase);
/// };
/// \note Only use this if you don't want to do any setup in the fixtures constructor.
/// \endcode
#define SQTEST_IMPLEMENT_FORWARDING_CONSTRUCTOR(THIS_CLASS_TYPE, BASE_CLASS_TYPE) \
  template<typename ... ParameterTypes> \
  THIS_CLASS_TYPE(ParameterTypes && ... parameters) \
  : BASE_CLASS_TYPE(std::forward<ParameterTypes>(parameters) ...) \
  { \
  }
