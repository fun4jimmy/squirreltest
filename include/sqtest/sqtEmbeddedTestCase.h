#pragma once
//----------------------------------------------------------------------------------------------------------------------
// Copyright (c) 2014 James Whitworth
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//----------------------------------------------------------------------------------------------------------------------
#include <sqtest/sqtTest.h>
#include <sqtest/sqtTestFactory.h>
//----------------------------------------------------------------------------------------------------------------------

/// \defgroup EmbeddedTestCases Embedded Test Cases
/// Embedded test cases are test cases that are contained with in a string declared within the test executable.
/// The string is passed to the macro as the name of the test case and will be parsed registered by static
/// initialisation.
/// To aid in embedding a script in to the test executable there is a premake embed action. Given an input script
/// the action will generate a header file with a const SQChar * array containing the contents of the script that
/// can then be included by the executable.
/// \code
/// const SQChar *exampleTestCase =
///   _SC("test.shouldSucceed <- function()\n")
///   _SC("{\n")
///   _SC("  expect_true(true);\n")
///   _SC("}\n");
/// SQTEST_EMBEDDED_TEST_CASE(exampleTestCase);
/// \endcode
///@{

//----------------------------------------------------------------------------------------------------------------------
/// Base fixture for embedded tests, declares an embedded test case fixture and registers all tests for the test case.
//----------------------------------------------------------------------------------------------------------------------
#define SQTEST_EMBEDDED_TEST_CASE_(EMBEDDED_NAME, FIXTURE_NAME) \
  class EMBEDDED_NAME ## EmbeddedTest : public FIXTURE_NAME \
  { \
    friend class sqt::TestFactory<EMBEDDED_NAME ## EmbeddedTest>; \
    static const SQRESULT m_testCaseRegistered; \
  protected: \
    SQTEST_IMPLEMENT_FORWARDING_CONSTRUCTOR(EMBEDDED_NAME ## EmbeddedTest, FIXTURE_NAME); \
  }; \
  const SQRESULT EMBEDDED_NAME ## EmbeddedTest::m_testCaseRegistered = \
    sqt::TestFactory<EMBEDDED_NAME ## EmbeddedTest>::RegisterTestCaseString(#EMBEDDED_NAME, EMBEDDED_NAME)

//----------------------------------------------------------------------------------------------------------------------
/// Simple embedded test case for tests that only depend on the squirrel standard library.
//----------------------------------------------------------------------------------------------------------------------
#define SQTEST_EMBEDDED_TEST_CASE(EMBEDDED_NAME) SQTEST_EMBEDDED_TEST_CASE_(EMBEDDED_NAME, sqt::Test)

//----------------------------------------------------------------------------------------------------------------------
/// Embedded test case for tests that depend on custom fixture set up.
//----------------------------------------------------------------------------------------------------------------------
#define SQTEST_EMBEDDED_TEST_CASE_F(EMBEDDED_NAME, FIXTURE_NAME) SQTEST_EMBEDDED_TEST_CASE_(EMBEDDED_NAME, FIXTURE_NAME)

///@}
