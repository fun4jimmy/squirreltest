#pragma once
//----------------------------------------------------------------------------------------------------------------------
// Copyright (c) 2014 James Whitworth
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//----------------------------------------------------------------------------------------------------------------------
#include <memory>
//----------------------------------------------------------------------------------------------------------------------
#include <gtest/gtest.h>
//----------------------------------------------------------------------------------------------------------------------
#include <sqtest/sqtScript.h>
#include <sqtest/sqtScriptPredicateHelper.h>
//----------------------------------------------------------------------------------------------------------------------

namespace sqt
{
//----------------------------------------------------------------------------------------------------------------------
/// Used to register test fixtures derived from sqt::TestBase.
/// \note Derived fixtures must implement a constructor that takes a std::shared_ptr<const Script> argument that
/// is forwarded to sqt::TestBase.
//----------------------------------------------------------------------------------------------------------------------
template<class TestType>
class TestFactory : public testing::internal::TestFactoryBase
{
public:
  /// Test case registration functions.
  /// \note These must be called before RUN_ALL_TESTS otherwise the tests will not be run.
  ///@{
  /// Register all tests contained within script testCaseScript.
  static SQRESULT RegisterTestCaseScript(const char *testCaseName, std::shared_ptr<const Script> testCaseScript);
  /// Register all tests contained within script string testCaseString.
  static SQRESULT RegisterTestCaseString(const char *testCaseName, const SQChar *testCaseString);
  ///@}

protected:

  TestFactory(std::shared_ptr<const Script> script);
  virtual ~TestFactory();

  /// Creates an instance of TestType for the googletest framework to run.
  virtual testing::Test *CreateTest() override;

private:
  const std::shared_ptr<const Script> m_script;
};

} // namespace sqt

#include <sqtest/sqtTestFactory.inl>
